﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class BallController : MonoBehaviour {
	public float maxHoldSeconds = 3;
	public float kickForce = 1000;

	private Rigidbody rb;
	private bool kicked;
	private float kickStartTime;
	private float kickEndTime;
	private Vector3 kickDirection;
	private Vector3 originalPosition;

	void Start() {
		rb = GetComponent<Rigidbody>();
		kicked = false;
		originalPosition = transform.position;
	}
	
	// Update is called once per frame
	void OnMouseDown () {
		if (!kicked)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			Physics.Raycast(ray, out hit);
//			Debug.Log(hit.point);
//			Debug.Log(transform.position);
			kickStartTime = Time.time;
			kickDirection = (transform.position - hit.point).normalized;
		}
	}

	void OnMouseUp () {
		if (!kicked){
			kicked = true;
			kickEndTime = Time.time;
			float force = System.Math.Min(kickEndTime - kickStartTime, maxHoldSeconds) / maxHoldSeconds * kickForce;
			rb.AddForce(kickDirection * force);
		}
	}

	public bool IsFlying()
	{
		return kicked;
	}

	public float GetStartFlyTime()
	{
		return kickEndTime;
	}

	public void Setup()
	{
		rb.velocity = rb.angularVelocity = Vector3.zero;
		transform.rotation = Quaternion.identity;
		transform.position = originalPosition + new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, -10));
		kicked = false;
		CameraController.Setup(transform.position, originalPosition);
	}
	
}
