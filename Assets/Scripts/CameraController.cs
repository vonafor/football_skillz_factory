﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	
	public static Vector3 orginalPosition;

	// Use this for initialization
	void Start () {
		orginalPosition = transform.position;
	}

	public static void Setup(Vector3 ballPosition, Vector3 originalBallPosition)
	{
		var originalCameraDistance = Vector3.Distance(orginalPosition.XZPlane(), originalBallPosition.XZPlane());
		Camera.main.transform.position = ballPosition.XZPlane() + Vector3.up * orginalPosition.y +
		                                 originalCameraDistance *
		                                 (ballPosition.XZPlane() - GoalZoneController.OriginalPosition.XZPlane()).normalized;
		SetupAngle(ballPosition);
	}

	static void SetupAngle(Vector3 ballPosition)
	{
		var goalZonePoint = GoalZoneController.OriginalPosition.XZPlane();
		var ballPoint = ballPosition.XZPlane();
		var angle = Vector3.Angle(goalZonePoint - ballPoint, Vector3.right);
		Camera.main.transform.rotation = Quaternion.Euler(0, 90 - angle, 0);
	}

}
