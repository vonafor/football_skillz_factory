﻿using System;
using UnityEngine;

public class GoalZoneController : MonoBehaviour
{
    public static Vector3 OriginalPosition;
    private ScoreManager scoreManager;
    private Collider collider;

    void Start()
    {
        OriginalPosition = transform.position;
        scoreManager = FindObjectOfType<ScoreManager>();
        collider = GetComponent<Collider>();
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Ball"))
        {
            var goalPoint = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            var goalType = ScoreManager.GoalType.Basic;
            if (Math.Abs(goalPoint.x - transform.position.x) / collider.bounds.size.x * 2 > 0.8f)
            {
                goalType = ScoreManager.GoalType.Side;
            }

            if (goalPoint.y > transform.position.y)
            {
                if ((goalPoint.y - transform.position.y) / collider.bounds.size.y * 2 > 0.7f)
                {
                    goalType = goalType == ScoreManager.GoalType.Side
                        ? ScoreManager.GoalType.Nine
                        : ScoreManager.GoalType.Top;
                }
            }
            
            scoreManager.ScoreGoal(goalType);
        }
    }
}
