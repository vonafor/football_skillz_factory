﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WallManager : MonoBehaviour {
	
	public int defendersCount = 2;
	private List<GameObject> defenders;
	public GameObject defenderPrefab;

	// Use this for initialization
	void Start () {
		defenders = new List<GameObject>();
	}
	
	List<Vector3> GetDefenderPositions()
	{
		var ball = FindObjectOfType<BallController>();
		if (ball == null)
		{
			return null;
		}
		var ballPoint = ball.transform.position.XZPlane();
		var zonePoint = GoalZoneController.OriginalPosition.XZPlane();
		var direction = (zonePoint - ballPoint).normalized;
		var orthogonalDirection = Quaternion.Euler(0, 90, 0) * direction;
		
		var wallStartPosition = ballPoint + direction * Random.Range(20, 25);
		wallStartPosition.y = 0.4f;
		var defenderPositions = new List<Vector3>();
		for (int i = -defendersCount / 2; i < defendersCount / 2 + 1; i++)
		{
			defenderPositions.Add(wallStartPosition + orthogonalDirection * 3 * i);
		}

		return defenderPositions;
	}

	public void Setup()
	{
		var defenderPositions = GetDefenderPositions();
		if (defenderPositions == null)
		{
			return;
		}
		if (!defenders.Any())
		{
			for (int i = 0; i < defendersCount; i++)
			{
				defenders.Add(Instantiate(defenderPrefab, defenderPositions[i], Quaternion.identity));
			}
		}
		else
		{
			for (int i = 0; i < defendersCount; i++)
			{
				var defender = defenders[i];
				defender.transform.position = defenderPositions[i];
				defender.transform.rotation = Quaternion.identity;
				var defenderRigidbody = defender.GetComponent<Rigidbody>();
				defenderRigidbody.velocity = defenderRigidbody.angularVelocity = Vector3.zero;
			}
		}
	}
}
