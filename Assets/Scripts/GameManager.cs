﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public float gameDuration = 10;
	private float startTime;

	public float maxBallFlyTime = 3;
	public GameObject ballPrefab;

	private GameObject ballObject;
	private GameObject gameOverMenu;
	private ScoreManager scoreManager;
	private BallController ballController;
	private WallManager wallManager;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
		ballObject = Instantiate(ballPrefab);
		ballController = (BallController)ballObject.GetComponent(typeof(BallController));
		gameOverMenu = GameObject.Find("GameOverMenu");
		gameOverMenu.SetActive(false);
		scoreManager = FindObjectOfType<ScoreManager>();
		wallManager = FindObjectOfType<WallManager>();
		wallManager.Setup();
	}
	
	// Update is called once per frame
	void Update () {
		if (ballController.IsFlying())
		{
			if (Time.time - ballController.GetStartFlyTime() > maxBallFlyTime)
			{
				ballController.Setup();
				wallManager.Setup();
			}
		}

		if (GetRemainingTime() < 0)
		{
			ballObject.SetActive(false);
			scoreManager.UpdateTopScore();
			gameOverMenu.SetActive(true);
		}
	}

	public float GetRemainingTime()
	{
		return gameDuration - (Time.time - startTime);
	}
}
