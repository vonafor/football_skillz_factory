﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour
{

	private float startTime;
	private Text text;
	private GameManager gameManager;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		gameManager = FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float remainingTime = gameManager.GetRemainingTime();
		if (remainingTime > 0)
		{
			text.text = ((int)remainingTime).ToString();
		}
		else
		{
			text.text = "";
		}
	}
}
