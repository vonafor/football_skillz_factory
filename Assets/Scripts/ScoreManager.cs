﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

	private int currentScore;
	private int maxScore;
	private Text text;
	
	public enum GoalType {Basic, Nine, Side, Top}

	private void Start()
	{
		text = GetComponent<Text>();
		maxScore = PlayerPrefs.GetInt("maxScore", 0);
	}

	public void ScoreGoal(GoalType type)
	{
		int score = 10;
		switch (type)
		{
				case GoalType.Nine:
					score = 300;
					break;
				case GoalType.Side:
					score = 50;
					break;
				case GoalType.Top:
					score = 100;
					break;
		}
		currentScore += score;
	}

	// Update is called once per frame
	void Update ()
	{
		text.text = String.Format("Score: {0}\nMax score: {1}", currentScore, currentScore > maxScore ? currentScore : maxScore);
	}

	public int GetScore()
	{
		return currentScore;
	}

	public void UpdateTopScore()
	{
		if (currentScore > maxScore)
		{
			PlayerPrefs.SetInt("maxScore", currentScore);
		}
	}
}
