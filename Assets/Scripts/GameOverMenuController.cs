﻿using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameOverMenuController : MonoBehaviour {

	public void RestartGame()
	{
		Debug.Log("Restart game");
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void QuitGame()
	{
		Debug.Log("Quit game");
		Application.Quit();
	}

	private void OnEnable()
	{
		var text = GetComponentInChildren<Text>();
		var scoreManager = FindObjectOfType<ScoreManager>();
		text.text = "Congratulation! Your score: " + scoreManager.GetScore();
	}
}
